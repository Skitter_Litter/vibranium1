
package net.mcreator.vibranium.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.SwordItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

import net.mcreator.vibranium.VibraniumModElements;

@VibraniumModElements.ModElement.Tag
public class VibraniumSwordItem extends VibraniumModElements.ModElement {
	@ObjectHolder("vibranium:vibranium_sword")
	public static final Item block = null;
	public VibraniumSwordItem(VibraniumModElements instance) {
		super(instance, 10);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new SwordItem(new IItemTier() {
			public int getMaxUses() {
				return 100;
			}

			public float getEfficiency() {
				return 4f;
			}

			public float getAttackDamage() {
				return 7f;
			}

			public int getHarvestLevel() {
				return 3;
			}

			public int getEnchantability() {
				return 17;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(VibraniumItem.block));
			}
		}, 3, -2.7999999999999998f, new Item.Properties().group(ItemGroup.COMBAT)) {
		}.setRegistryName("vibranium_sword"));
	}
}
