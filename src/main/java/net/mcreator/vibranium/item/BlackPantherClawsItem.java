
package net.mcreator.vibranium.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;
import net.minecraft.item.AxeItem;

import net.mcreator.vibranium.VibraniumModElements;

@VibraniumModElements.ModElement.Tag
public class BlackPantherClawsItem extends VibraniumModElements.ModElement {
	@ObjectHolder("vibranium:black_panther_claws")
	public static final Item block = null;
	public BlackPantherClawsItem(VibraniumModElements instance) {
		super(instance, 36);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new AxeItem(new IItemTier() {
			public int getMaxUses() {
				return 2000;
			}

			public float getEfficiency() {
				return 9.5f;
			}

			public float getAttackDamage() {
				return 10f;
			}

			public int getHarvestLevel() {
				return 3;
			}

			public int getEnchantability() {
				return 0;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(VibraniumItem.block));
			}
		}, 1, -3.2000000000000001f, new Item.Properties().group(ItemGroup.COMBAT)) {
		}.setRegistryName("black_panther_claws"));
	}
}
