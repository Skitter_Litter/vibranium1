
package net.mcreator.vibranium.item;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

import net.mcreator.vibranium.VibraniumModElements;

@VibraniumModElements.ModElement.Tag
public class VibraniumPickaxeItem extends VibraniumModElements.ModElement {
	@ObjectHolder("vibranium:vibranium_pickaxe")
	public static final Item block = null;
	public VibraniumPickaxeItem(VibraniumModElements instance) {
		super(instance, 11);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new PickaxeItem(new IItemTier() {
			public int getMaxUses() {
				return 2500;
			}

			public float getEfficiency() {
				return 12f;
			}

			public float getAttackDamage() {
				return 5f;
			}

			public int getHarvestLevel() {
				return 4;
			}

			public int getEnchantability() {
				return 17;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(VibraniumItem.block));
			}
		}, 1, -2.9000000000000001f, new Item.Properties().group(ItemGroup.TOOLS)) {
		}.setRegistryName("vibranium_pickaxe"));
	}
}
